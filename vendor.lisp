(in-package vendor)

(defvar *init-file*
  #+sbcl
  ".sbclrc"
  #+(and ccl windows)
  "ccl-init.lisp"
  #+(and ccl (not windows))
  ".ccl-init.lisp")
#|
    (:implementation allegro
    ".clinit.cl")
  (:implementation abcl
    ".abclrc")
  (:implementation ccl
    #+windows
    "ccl-init.lisp"
    #-windows
    ".ccl-init.lisp")
  (:implementation clisp
    ".clisprc.lisp")
  (:implementation ecl
    ".eclrc")
  (:implementation lispworks
    ".lispworks")
  (:implementation sbcl
    ".sbclrc")
  (:implementation cmucl
    ".cmucl-init.lisp")
  (:implementation scl
    ".scl-init.lisp")
  )
|#
       

(defun write-to-init-file (code)
  (with-open-file (stream *init-file* :direction :output :if-exists :append)
    (write-string code stream)
  ))
(export 'write-to-init-file)

(defun write-vendor-to-init-file ()
  (write-to-init-file (format nil "~%(require 'cl-vendor)~%")))
(export 'write-vendor-to-init-file)

(defmacro error-platform-unsupported ()
  `(error "This platform is not supported"))

(defun quit (&optional (exit-code 0))
  (declare (ignorable exit-code))
  #+sbcl
  (sb-ext:quit)
  #+ccl
  (ccl:quit)
  #-(or sbcl ccl)
  (error-platform-unsupported)
  )
(export 'quit)


(defun generate-exe (filename toplevel-fn)
  #+ccl
  (progn
    (ccl:save-application filename
			  :toplevel-function toplevel-fn
			  :error-handler :quit
			  :prepend-kernel t)
    (quit)
    )

  #+sbcl
  (sb-ext:save-lisp-and-die filename
			    :toplevel toplevel-fn
			    :executable t)

  #-(or sbcl ccl)
  (error-platform-unsupported)
  )  
(export 'generate-exe)

(defmacro eval-when-all (&body body)
  `(eval-when (:compile-toplevel :load-toplevel :execute)
     ,@body))

(export 'eval-when-all)
