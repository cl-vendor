
(defpackage cl-vendor-asd
  (:use :cl :asdf))

(in-package cl-vendor-asd)

(defsystem cl-vendor
  :name "cl-vendor"
  :author "Charles Lew(crlf0710@gmail.com)"
  :maintainer "Charles Lew(crlf0710@gmail.com)"
  :license "MIT"
  :description "A library for hiding implementation differences"
  :depends-on (:trivial-features)
  :components
  ((:file "package")
   (:file "impl" :depends-on ("package"))
   (:file "vendor" :depends-on ("impl"))
   )
  )

