
(in-package vendor)

(defclass impl-info ()
  ((impl-index          :initarg :index)
   (impl-feature-index  :initarg :feature-word)
   (impl-iname-index    :initarg :iname)
   (impl-iver-index      :initarg :iver)))

(defparameter *lisp-implementations* nil)

(defun define-lisp-implementation (impl-keyword information word-iname-iver-list &optional aux-fn)
  (pushnew (list impl-keyword information word-iname-iver-list aux-fn)
	   *lisp-implementations* ))

(define-lisp-implementation :legal-common-lisp "A legal common lisp implementation." nil)

(define-lisp-implementation :sbcl "Steel Bank Common Lisp - http://www.sbcl.org/"
  '((:sbcl (("SBCL" "1.0.53.74.mswinmt.1092-207a13d")))))

(define-lisp-implementation :ccl  "Clozure Common Lisp - http://ccl.clozure.com"
  '((:ccl (("Clozure Common Lisp" "Version 1.7-r15116M  (WindowsX8632)")))))

(defun lisp-implementation-info ()
  (loop with impl-info = nil
     for (impl-name information word-iname-iver-list aux-fn) in *lisp-implementations*
     until impl-info
     do (loop for (word iname-iver-list) in word-iname-iver-list
	   until impl-info
	   do (when (find word *features*)
		(let* ((iname-list (or (find (lisp-implementation-type)
					     iname-iver-list :key #'car :test #'string=)
				       (car (last iname-iver-list))))
		       (iver  (if aux-fn
				  (funcall aux-fn word (car iname-list)
					   (cdr iname-list) (lisp-implementation-version))
				  (position (lisp-implementation-version)
					    (cdr iname-list) :test #'string=))))
		  (setf impl-info
			(make-instance 'impl-info
				       :index (position impl-name *lisp-implementations*
							:key #'car)
				       :feature-word (position word word-iname-iver-list
							       :key #'car)
				       :iname (position (lisp-implementation-type)
							iname-iver-list :key #'car
							:test #'string=)
				       :iver  iver)))))
     finally (return (if impl-info impl-info
			 (make-instance 'impl-info
					:index (1- (length *lisp-implementations*))
					:feature-word nil :iname nil :iver nil)))))

(defmethod impl-name ((impl-info impl-info))
    (let* ((lisp-impl-info
	    (nth (slot-value impl-info 'impl-index) *lisp-implementations*)))
      (assert (symbolp (first lisp-impl-info)))
      (symbol-name (first lisp-impl-info))))

(defun get-implementation-name ()
  (impl-name (lisp-implementation-info)))
(export 'get-implementation-name)

(defmethod print-object ((impl-info impl-info) stream)
  (print-unreadable-object (impl-info stream :type 'impl-info)
    (let* ((lisp-impl-info
	    (nth (slot-value impl-info 'impl-index) *lisp-implementations*))
	   (lisp-feature-keyword
	    (and (slot-value impl-info 'impl-feature-index)
		 (nth (slot-value impl-info 'impl-feature-index)
		      (third lisp-impl-info))))
	   (lisp-iname-iver-list
	    (and (slot-value impl-info 'impl-iname-index)
		 (nth (slot-value impl-info 'impl-iname-index)
		      (cadr lisp-feature-keyword))))
	   (lisp-iver-item
	    (and (slot-value impl-info 'impl-iver-index)
		 (nth (1+ (slot-value impl-info 'impl-iname-index))
		      lisp-iname-iver-list)))
	   )
      (format stream
	      "~a~@[[:~a]~]~@[(~a)~]~@[~a~]"
	      (first lisp-impl-info)
	      (car lisp-feature-keyword)
	      (car lisp-iname-iver-list)
	      lisp-iver-item)
      )))

